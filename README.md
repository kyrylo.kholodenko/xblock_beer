XBlockBeer
============

The XBlock provide opportunity to get information about beer.


Installation
------------

```
pip install -e git+https://gitlab.com/kyrylo.kholodenko/xblock_beer/
```

Usage
-----

* Add ```beerblock``` to advanced modules in the advanced settings of a course.
* Write the name of the beer in the input field and click on the search button.

![Menu](doc/img/xblockbeer_menu.jpg)


![View](doc/img/xblockbeer_view.jpg)
