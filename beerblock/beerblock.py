"""TO-DO: Write a description of what this XBlock is."""

import pkg_resources
from web_fragments.fragment import Fragment
from xblock.core import XBlock
from xblock.fields import Integer, Scope, String


class BeerXBlock(XBlock):
    """
    TO-DO: document what your XBlock does.
    """

    # Fields are defined on the class.  You can access them in your code as
    # self.<fieldname>.
    user_input = String(help="", default="")

    display_name = String(
        display_name=("Info Beer"),
        default=("Info Beer"),
        scope=Scope.settings,
        help=("This name appears in the horizontal navigation at the top of the page.")
    )

    def resource_string(self, path):
        """Handy helper for getting resources from our kit."""
        data = pkg_resources.resource_string(__name__, path)
        return data.decode("utf8")

    # TO-DO: change this view to display your data your own way.
    def student_view(self, context=None):
        """
        The primary view of the BeerXBlock, shown to students
        when viewing courses.
        """
        context = {"display_name":self.display_name}
        html = self.resource_string("static/html/beerblock.html")
        print("FSAFASFSAFASFAS", dir(self))
        frag = Fragment(html.format(self=self))
        frag.add_css(self.resource_string("static/css/beerblock.css"))
        frag.add_javascript(self.resource_string("static/js/src/beerblock.js"))
        frag.initialize_js('BeerXBlock')
        return frag

    # TO-DO: change this to create the scenarios you'd like to see in the
    # workbench while developing your XBlock.
    @staticmethod
    def workbench_scenarios():
        """A canned scenario for display in the workbench."""
        return [
            ("BeerXBlock",
             """<beerblock/>
             """),
            ("Multiple BeerXBlock",
             """<vertical_demo>
                <beerblock/>
                <beerblock/>
                <beerblock/>
                </vertical_demo>
             """),
        ]
