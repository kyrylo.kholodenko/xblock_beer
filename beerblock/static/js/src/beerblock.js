let page = 0

/* Javascript for BeerXBlock. */
function BeerXBlock(runtime, element) {
    var $element = $(element);

    function printAllBeers(beers){
        $.getScript("https://pagination.js.org/dist/2.1.5/pagination.min.js")

        beers.forEach(beer => {
            $element.find(".beersdiv").append(`<div class="item" id="${beer['id']}"></div>`);
            $element.find(`#${beer['id']}`).append(`<img src="${beer['image_url']}" alt="beer">`);
            $element.find(`#${beer['id']}`).append(`<div class="item-text" id="${beer['id']}-text"></div>`);
            $element.find(`#${beer['id']}-text`).append(`<p><b>${beer['name']}</b></p>`);
            $element.find(`#${beer['id']}-text`).append(`<p>${beer['description']}</p>`);
            $element.find(`#${beer['id']}-text`).append("<br><br>");
        })
    }

    function fetchingBeers() {
        var $input = $element.find('.user_input');
        var $button = $element.find('.save-answer');
        page += 1

        $button.addClass('disabled');
        $element.find('.feedback-message').text('finding...');

        $.ajax({
            type: "GET",
            url: `https://api.punkapi.com/v2/beers?page=${page}&per_page=3&beer_name=`+$input.val(),
            success: printAllBeers
        })
            .done(function (response) {
                $button.removeClass('disabled');
                $element.find('.feedback-message').text('');
                $element.find('.load-more').removeAttr('hidden')
            })
            .fail(function () {
                $element.find('.feedback-message')
                    .addClass('error')
                    .text('An error occurred while saving. Please, try again later.');
                $button.removeClass('disabled');
            });
    }

    function clearAll(){
        $element.find(".beersdiv").empty()
        $element.find(".load-more").setAttribute('hidden')
        page = 0
    }

    $element.find(".save-answer").click(fetchingBeers);
    $element.find(".load-more").click(fetchingBeers);
    $element.find(".clear-all").click(clearAll)
}